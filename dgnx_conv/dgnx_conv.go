
/*
	Abstract:	Iod_conv is a conversion programme which reads XML
				files created by io.Draw and generated encapulated
				postscript output.

	Date:		29 May 2022
	Author:		E. Scott Daniels
*/

package main

import (
	"flag"
	"fmt"
	"io"
	"os"
	"strings"

	dgnx "gitlab.com/rouxware/dgnx2ps/ioxml2eps"
)

func abortOnErr( err error, msg string ) {
	if err != nil {
		fmt.Fprintf( os.Stderr, "[FAIL] %s: %s\n", msg, err )
		os.Exit( 1 )
	}
}

/*
	Open and truncate a file.
*/
func openTrunc(fname string) (io.Writer, error) {
	file, err := os.OpenFile(fname, os.O_WRONLY|os.O_CREATE|os.O_TRUNC, 0644)
	return file, err
}


/*
	Creates a new output file name and opens it. The file is named:
		<base>[_<page].<suffix> 
	Page is dropped if 0. The base may be the string "stdout" or
	"-" to indicate output to standard out.
*/
func openOutput( base string, suffix string, page int ) io.Writer {
	if base == "stdout" || base == "-" {
		return os.Stdout
	}

	var fName string
	tokens := strings.Split( base, "." )
	if len(tokens) > 1 && tokens[len(tokens)-1] == suffix {		// already there
		fName = base
	} else {
		if page > 0 {
			fName = fmt.Sprintf( "%s_%d.%s", base, page, suffix )
		} else {
			fName = fmt.Sprintf( "%s.%s", base, suffix )
		}
	}

	out, err := openTrunc( fName )
	abortOnErr( err, fmt.Sprintf( "cannot open output %q", fName ) )

	fmt.Fprintf( os.Stderr, "opened: %s\n", fName )
	return out
}

// --------------------------------------------------------------------------

func main() {
	var version string = "2.0"
	var showVersion bool = false
	var page int = 0
	var allPages bool = false
	var genPs bool = false
	var outFname string
	var verbose bool = false
	var invertColours = false

	out := io.Writer( os.Stdout )

	flag.IntVar( &page, "p", 1, "page to convert" )
	flag.BoolVar( &allPages, "all", false, "print all pages from -p page to end" )
	flag.BoolVar( &invertColours, "invert", false, "assume dark background inverting colours where possible" )
	flag.StringVar( &outFname, "o", "stdout", "output file name (without exension)" )
	flag.BoolVar( &genPs, "ps", false, "generate postscript rather than EPS" )
	flag.BoolVar( &verbose, "v", false, "verbose mode" )
	flag.BoolVar( &showVersion, "version", false, "show version and exit" )
	flag.Parse()

	if invertColours {
		dgnx.InvertColours( true )
	}

	if showVersion {
		fmt.Fprintf( os.Stdout, "dgmx_conv version %s\n", version )
		os.Exit( 0 )
	}

	if flag.NArg() < 1 {
		fmt.Fprintf( os.Stderr, "[FAIL] missing input (xml) file name on command line\n" )
		os.Exit( 1 )
	}
	fName := flag.Arg(0)

	if allPages && !genPs {
		if outFname == "stdout" {
			fmt.Fprintf( os.Stderr, "[FAIL] cannot use stdout (default) with -all flag; use -o to set output base name" )
			os.Exit( 1 )
		}
	}

	drawing, err := dgnx.NewSketch( fName )				// laod the drawing from disk
	abortOnErr( err, fmt.Sprintf( "xml file %q could not be loaded", fName) )

	nPages := drawing.Pages()
	if page > nPages {
		fmt.Fprintf( os.Stderr, "[FAIL] requested page (%d) does not exist (nPages=%d)\n", page, nPages )
		os.Exit(1)
	}

	for {
		if genPs {	// set up for postscript -- just once as it all dumps into the same file
			if page == 1 {

				out = openOutput( outFname, "ps", 0 )
				fmt.Fprintf( out, "%%!PS-Adobe-3.0\n" )
				fmt.Fprintf( out, "%%Creator: dgnx_conv v1.0\n" )
				fmt.Fprintf( out, "%% <INFO> drawing loaded from %s\n", fName )
				fmt.Fprintf( out, "%% <INFO> page %d of %d; %d total elements\n", page, nPages, drawing.EleCount() )
			} else {
				fmt.Fprintf( out, "showpage\n" )
			}

			if invertColours {
				fmt.Fprintf( out, "0 0 0 setrgbcolor 0 0 moveto 700 0 rlineto 0 792 rlineto -700 0 rlineto closepath fill\n" )
			}

			scaleX, scaleY := drawing.PageScale()
			fmt.Fprintf( out, "0 792 translate\n" )		// translate to the top of the page
			fmt.Fprintf( out, "\n%.2f %.2f scale\n\n", scaleX, scaleY )
		} else {
			if allPages {
				out = openOutput( outFname, "eps", page )	// new file for each page when in eps mode
			} else {
				out = openOutput( outFname, "eps", 0 )
			}

			minPt, maxPt := drawing.BoundingBox()
			fmt.Fprintf( out, "%%!PS-Adobe-3.0 EPSF-3.0\n" )
			fmt.Fprintf( out, "%%%%Creator: dgnx_conv v1.1\n" )
			fmt.Fprintf( out, "%%%%BoundingBox: %s %s\n", minPt, maxPt );
			fmt.Fprintf( out, "%% <INFO> drawing loaded from %s\n", fName )
			fmt.Fprintf( out, "%% <INFO> page %d of %d; %d total elements\n", page, nPages, drawing.EleCount() )

			if invertColours {
				minX, minY := minPt.XY()
				maxX, maxY := maxPt.XY()
				fmt.Fprintf( out, "0 0 0 setrgbcolor %d %d moveto %d 0 rlineto 0 %d rlineto -%d 0 rlineto closepath fill\n", 
					minX, minY, maxX, maxY, maxX )
			}

			_, y := maxPt.XY()
			fmt.Fprintf( out, "0 %d translate\n", y )	// translate to top of bounding box
		}

		drawing.SelectSketch( page-1 )

		nEles := drawing.EleCount()
		if verbose {
			fmt.Fprintf( os.Stderr, "page %d has %d elements", page, nEles )
		}

		for i := 0; i < nEles; i++ {
			ele := drawing.Element( i )
			if verbose {
				fmt.Fprintf( os.Stderr, "%% element %d is a %v\n", i, ele.Kind() )
			}
			ele.Paint( out )
		}

		if !allPages || ! drawing.NextSketch() { // to next page
			break
		}
		page++
	}
	
	fmt.Fprintf( out, "%%EOF\n" )	// mostly for EPS, but won't hurt in ps mode
}
