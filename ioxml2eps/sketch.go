/*
	Abstract:	The sketch is the object which manages the underlying IO
				xml goop. It provides the API to the user programme providing
				a bit of insulation from the underlying madness in the XML.

				One of the must bizarre things about the XML is the fact that
				a line has a defined source and target point, complete with
				a value, but this value isn't accurate if in the drawing
				the line is "attached" to another object. From an interactive
				point of view, this makes sense as it allows the point to follow
				the movement of the object, but when rendering the line these
				end points must be ignored, and the real values computed based
				on an x,y "precentage" relative to the attached drawing thing's
				dimensions.

				To deal with this, um, feature, we must map the drawing things
				so when we need to paint a line we can look up and compute the
				"attachment" point.

	Date:		17 March 2022
	Author:		E. Scott Daniels
*/
package ioxml2eps

import (
	"encoding/xml"
)

type Sketch struct {
	sketches	*DFile				// manages a list of "diagrams" from the file
	cur			*Diagram			// the current sketch within the file
	page		int					// might have a page concept, allow selection of "current"
	dThings		map[string]*DThing		// map of all elements across all pages
	pgDtMaps	[]map[string]*DThing	// map of elements on each page
}

/*
	Opens the named file and loads the XML drawing. After it is loaded, all pages are searched
	to create a map of the drawing things. The map is traversed to set any references that the
	drawing things have to other DTs.
*/
func NewSketch( fname string ) (*Sketch, error) {
	goop, err := readFile( fname )
	if err != nil {
		return nil, err
	}

	stuff := &DFile{}					// empty space to fill in
	err = xml.Unmarshal( goop, stuff )
	if err != nil {
		return nil, err
	}

	sk := &Sketch {
		sketches:	stuff,
		cur:		stuff.Page(),	// prime with first "page"
	}

	eMap := make( map[string]*DThing )
	pMaps := make( []map[string]*DThing, 0 )
	more := true
	for ; more; {
		curPMap := make( map[string]*DThing, 0 )

		nEles := sk.EleCount()			// elements on this page
		for i := 0; i < nEles; i++ {	// we assume IDs are unique across pages
			ele := sk.Element( i )
			if ele != nil {
				eMap[ele.Id] = ele
				curPMap[ele.Id] = ele
			}
		}

		more = sk.NextSketch()

		pMaps = append( pMaps, curPMap )
	}

	for _, dThing := range eMap {
		sID, tID := dThing.Attachments()	// get the IDs of what the dThing references as attachments
		s, known := eMap[sID]
		if !known {
			s = nil
		}
		t, known := eMap[tID]
		if !known {
			t = nil
		}
		dThing.AddAttachments( s, t )		// and add the direct reference
	}

	sk.dThings = eMap
	sk.pgDtMaps = pMaps
	sk.SelectPage( 0 )

	return sk, nil
}

// ------------ page oriented things -------------------------------------------
/*
	Select the first sketch in the file.
*/
func (sk *Sketch) FirstSketch() {
	if sk == nil {
		return
	}

	sk.SelectPage( 0 )
}

/*
	Select the desired sketch from the file.
*/
func (sk *Sketch) SelectSketch( p int ) {
	if sk == nil {
		return
	}

	sk.sketches.Select( p )		// first set it then,
	sk.cur = sk.sketches.Page()	// pull it as current
}

/*
	Select the next page; return false if no more pages.
*/
func (sk *Sketch) NextSketch() bool {
	if sk == nil {
		return false
	}

	p := sk.sketches.NextPage()
	if p != nil {					// if there is one
		sk.cur = p
		return true
	}

	return false
}

// ------ current page oriented things ------------------------------------------
/*
	Returns the sketch element 'e' or nil if e is out of range.
*/
func (sk *Sketch) Element( e int ) *DThing {
	if sk == nil {
		return nil
	}
	if e < 0 || e > sk.EleCount() {
		return nil
	}

	dg := sk.cur
	return &dg.Page[sk.page].Slate.Things[e]
}

/*
	Returns the bounding box which contains all of the elements.
*/
func (sk *Sketch) BoundingBox() (minPt *RealPoint, maxPt *RealPoint) {
	if sk == nil {
		return NewRealPoint(0, 0), NewRealPoint(0, 0)
	}

	pts := make( []*RealPoint, 0 )
	pMap := sk.pgDtMaps[sk.page]	// look only at elements on the current page
	for _, dt := range( pMap ) {	// gather the min/max points for each dThing
		pt1, pt2 := dt.BoundingBox()
		pts = append( pts, pt1 )
		pts = append( pts, pt2 )
	}

	minPt, maxPt = BoundingBox( pts )	// run the array of points to find min/max
	return minPt, maxPt
}

/*
	Return the count of elements.
*/
func (sk *Sketch) EleCount() int {
	if sk == nil {
		return 0
	}

	dg := sk.cur
	if dg == nil {
		return 0
	}
	if len( dg.Page ) == 0 {
		return 0
	}

	eles := dg.Page[sk.page].Slate.Things
	return len( eles )
}

/*
	Returns the x,y scale for the page.
*/
func (sk *Sketch) PageScale( ) (float64, float64) {
	if sk == nil {
		return 1.0, 1.0
	}

	dg := sk.cur
	if dg == nil {
		return 1.0, 1.0
	}
	if len( dg.Page ) == 0 {
		return 1.0, 1.0
	}
	return dg.Page[0].LetterScale()	// future: allow for different page sizes
}

/*
	Returns the number of pages in the drawing.
*/
func (sk *Sketch) Pages( ) int {
	if sk == nil {
		return 0
	}

	return sk.sketches.PageCount()
}

/*
	If the drawing has multiple pages, allow the desird page to be
	selected. If p goes out of bounds, no change is made.
*/
func (sk *Sketch) SelectPage( p int ) {
	if sk == nil {
		return
	}

	dg := sk.cur
	if dg == nil {
		return 
	}
	pages := len( dg.Page )
	if p >= 0 && p < pages {
		sk.page = p
	}
}

