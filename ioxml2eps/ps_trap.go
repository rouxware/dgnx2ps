/*
	Abstract:	A trapizoid. Base is wider, and the style size variable is the
				number of points that each top corner is "inset".

	Date:		12 Apr 2024
	Author:		E. Scott Daniels
*/

package ioxml2eps

import (
	"io"
)

var psTrapWritten bool = false
var psTrapCode string = `
	% create the path of the trapizoid
    % x y width height inset trapizoid path
	/trapizoid {
		/_trinset ed
		newpath
		4 2 roll exch _hxinset add exch moveto	% left top edge; width height
		exch dup _hxinset 2 mul sub				% height width top-width
		0 rlineto exch dup          		% width height height
		_trinset exch neg rlineto			% right side	width height
		exch neg 0 rlineto					% base
		closepath
		pop
	} def


	% filled trapizoid thing; stroking with current colour
	% r g b x y width height tailPos ftrapizoid
	/ftrapizoid {
		5 copy trapizoid	% dup everything and set the path
		gsave
			8 5 roll
			setrgbcolor	
			fill
		grestore
		trapizoid
		stroke
	} def

	% fill only trapizoid thing (no stroke)
	% r g b x y width height tailPos fotrapizoid
	/fotrapizoid {
		trapizoid
		gsave
			setrgbcolor
			fill
		grestore
	} def
`

func psTrapWrite( out io.Writer ) {
	if ! psTrapWritten {
		psUtilWrite( out )	// ensure we are set up
		out.Write( squishPS( psTrapCode, true ) )
		psTrapWritten = true
	}
}
