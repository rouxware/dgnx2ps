/*
	Abstract:	Postscript support utilities.

	Date:		25 April 2022
	Author:		E. Scott Daniels
*/

package ioxml2eps

import (
	"fmt"
	"io"
	"strings"
)

var psUtilWritten bool = false
var psUtilCode = `
	/ed { exch def } def
	/pg { showpage } def

	/gsv { gsave  } def
	/grst{ grestore } def
	/setrgbc{ setrgbcolor } def

	/sfsf { scalefont setfont } def
	%  size name fnt
	/fnt { findfont sfsf } def

	/black { 0 0 0  } def
	/white { 1 1 1 } def
	/shadow { .80 .80 .80  } def
	/setBlack { black setrgbcolor } def
	/setWhite { white  setrgbcolor } def
	/setShadow { shadow setrgbcolor } def

	% "parameter" snarfing
	/snagXY { /_y exch def /_x exch def } def
	/snagXYWH { /_h exch def /_w exch def snagXY } def
	/snagWH { /_h exch def /_w exch def } def
	/snagRGB { /_b exch def /_g exch def /_r exch def } def
`

/*
	Finds the first <space>% or <tab>
*/
func findComment( s string ) int {
	if s[0] == '%' {
		return 0
	}
	i := strings.Index( s, " %" )
	if i >= 0 {
		return i
	}
	return strings.Index( s, "\t%" )
}

/*
	Escape characters in raw that can not go into a
	postscript (<string>) statement without special
	treatment.
*/
func psEscape( raw string ) string {
	cooked := strings.ReplaceAll( raw, "\\", "\\\\" )
	cooked = strings.ReplaceAll( cooked, "(", "\\(" )
	cooked = strings.ReplaceAll( cooked, ")", "\\)" )

	return cooked
}

/*
	Squish a postscript code segment into something we can put out
	(strip comments and some whitespace, reduce some long patterns
	to abbreviations, etc.).

	Some readers do not "appreciate" buffers with long lines
	so we inject a newline every 200 bytes or so.
*/
func squishPS( ps string, abbrs bool ) []byte {
	ijCount := 0
	recs := strings.Split( ps, "\n" )
	buf := ""
	for _, r := range recs {
		if r == "" {
			continue
		}

		i := findComment( r )
		if i == 0 {
			continue
		}
		if i >= 0 {
			r = r[:i] + " "
		}
		r = strings.Trim( r, " \t" )
		if r != "" {
			if len(r) +  ijCount > 200 {
				ijCount = 0
				buf += "\n"
			} else {
				ijCount += len(r)
			}
			buf += r + " "
		}
	}

	if abbrs {
		buf = strings.ReplaceAll( buf, "scalefont setfont", "sfsf" )
		buf = strings.ReplaceAll( buf, "setrgbcolor", "setrgbc" )
		buf = strings.ReplaceAll( buf, "exch def", "ed" )
		buf = strings.ReplaceAll( buf, "gsave", "gsv" )
		buf = strings.ReplaceAll( buf, "grestore", "grst" )
	}
	buf += "\n"
	return []byte(buf)
}

/*
	Format the parameters into a "raw" string, then squish the string
	into bytes before writing to the output device.
*/
func psFprintf( dev io.Writer, ufmt string, args ...interface{}) {
	raw := fmt.Sprintf( ufmt, args... )
	cooked := squishPS( raw, true )
	dev.Write( cooked )
}


func psUtilWrite( out io.Writer ) {
	if ! psUtilWritten {
		out.Write( squishPS( psUtilCode, false ) )
		psUtilWritten = true
	}
}


