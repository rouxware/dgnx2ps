/*
	Abstract:	Postscript to support drawing a "disk".

	Date:		25 April 2022
	Author:		E. Scott Daniels
*/

package ioxml2eps

import (
	"io"
)

var psDiskWritten bool = false
var psDiskCode string = `
	% x y width height disk
	/disk {
		/_h exch def
		/_w exch def
		/_y exch def
		/_x exch def
		/_b exch def
		/_g exch def
		/_r exch def
		_r _g _b _x _y _w _h fbox stroke
		gsv
			%3 setlinewidth
			 _r _g _b _x _w 2 div add _y _h add _w 20 fellipse
		grst
		gsv
			_x _w 2 div add 
			_y  1 add
			_w 20 180 360 
			6 copy
			gsv 
				arcPath 
				_r _g _b setrgbcolor
				fill
			grst
			gsv
				arcPath
				1 setlinewidth
				stroke
			grst
		grst
	} def
`

func psDiskWrite( out io.Writer ) {
	if ! psDiskWritten {
		psUtilWrite( out )	// ensure we are set up
		out.Write( squishPS( psDiskCode, true ) )
		psDiskWritten = true
	}
}
