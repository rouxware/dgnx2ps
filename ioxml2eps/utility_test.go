
/*
	Abstract:	Utility functions which support unit testing
	Date:		2 April 2022
	Author:		E. Scott Daniels
*/

package ioxml2eps

import (
	"fmt"
	"os"
	"testing"
)


/*
	Fails the test, and writes message  if the condition is true. Returns
	true if the condition is true.
*/
func failIfTrue( t *testing.T, condition bool, msg string ) bool {
	if condition {
		fmt.Fprintf( os.Stderr, "<FAIL> %s\n", msg )
		t.Fail()
		return true
	}

	return false	
}

func abortIfTrue( t *testing.T, condition bool, msg string ) bool {
	if condition {
		fmt.Fprintf( os.Stderr, "<FAIL> %s\n", msg )
		t.Fail()
		os.Exit( 1 )
	}

	return false	
}

/*
	Fail the test if the condition is false. Returns true if the 
	condition is false.
*/
func failIfFalse( t *testing.T, condition bool, msg string ) bool {
	if !condition {
		fmt.Fprintf( os.Stderr, "<FAIL> %s\n", msg )
		t.Fail()
		return true
	}

	return false	
}

func abortIfFalse( t *testing.T, condition bool, msg string ) bool {
	if !condition {
		fmt.Fprintf( os.Stderr, "<FAIL> %s\n", msg )
		t.Fail()
		os.Exit( 1 )
	}

	return false	
}
