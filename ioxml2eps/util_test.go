
/*
	Abstract:	Utility function tests
	Date:		15 March 2022
	Author:		E. Scott Daniels
*/

package ioxml2eps

import (
	"fmt"
	"os"
	"testing"
)

func TestFileRead( t *testing.T ) {
	_, err := readFile( "test_data/dt_test.xml" )
	failIfTrue( t, err != nil, fmt.Sprintf( "failed to read file: %s", err ) )
	if err == nil {
		fmt.Fprintf( os.Stderr, "<INFO> file read ok\n" )
	}
}

func TestAtof( t *testing.T ) {
	v := atof( "4.3086" )
	failIfTrue( t, v != 4.3086, "atof was wrong" )

	v = atof( "-4.3086" )
	failIfTrue( t, v != -4.3086, "atof was wrong" )

	v = atof( "-4.3086%" )
	failIfTrue( t, v != -4.3086, "atof was wrong" )

	v = atof( ".3086" )
	failIfTrue( t, v != .3086, "atof was wrong" )
}

func TestRotate( t *testing.T ) {
	pt := NewRealPoint( 200, 100 )
	centre := NewRealPoint( 100, 100 )
	
	x,y := pt.XY()
	fmt.Fprintf( os.Stderr, "100 100 moveto %d %d lineto stroke\n", x, y )

	x, y = pt.Rotate( centre, 45 ).XY()
	fmt.Fprintf( os.Stderr, "100 100 moveto %d %d lineto stroke\n", x, y )

	fmt.Fprintf( os.Stderr, "1.0 0 0 setrgbcolor\n" )
	x, y = pt.Rotate( centre, 90 ).XY()
	fmt.Fprintf( os.Stderr, "100 100 moveto %d %d lineto stroke\n", x, y )

	fmt.Fprintf( os.Stderr, " 0 1.0 0 setrgbcolor\n" )
	x, y = pt.Rotate( centre, 120 ).XY()
	fmt.Fprintf( os.Stderr, "100 100 moveto %d %d lineto stroke\n", x, y )

	fmt.Fprintf( os.Stderr, "0 0 1.0 setrgbcolor\n" )
	x, y  = pt.Rotate( centre, 180 ).XY()
	fmt.Fprintf( os.Stderr, "100 100 moveto %d %d lineto stroke\n", x, y )

	fmt.Fprintf( os.Stderr, "1.0 1.00 0 setrgbcolor\n" )
	x, y   = pt.Rotate( centre, 270 ).XY()
	fmt.Fprintf( os.Stderr, "100 100 moveto %d %d lineto stroke\n", x, y )

	fmt.Fprintf( os.Stderr, "1.0 0 1.0 setrgbcolor\n" )
	x, y  = pt.Rotate( centre, 310 ).XY()
	fmt.Fprintf( os.Stderr, "100 100 moveto %d %d lineto stroke\n", x, y )
}

func TestPullBack( t *testing.T ) {
	pta := NewRealPoint( 100, 100 )	
	ptb := NewRealPoint( 200, 200 )	
	fmt.Fprintf( os.Stderr, "100 100 moveto 200 200 lineto stroke\n" )

	ptc := ptb.PullBack( pta, 25 )
	fmt.Fprintf( os.Stderr, "%s\n", ptc )

	ptb = NewRealPoint( 100, 100 )	
	pta = NewRealPoint( 200, 200 )	
	ptc = ptb.PullBack( pta, 25 )
	fmt.Fprintf( os.Stderr, "%s\n", ptc )

	fmt.Fprintf( os.Stderr, "200 200  moveto 100 150 lineto stroke\n" )
	pta = NewRealPoint( 200, 200 )	
	ptb = NewRealPoint( 100, 150 )	
	ptc = ptb.PullBack( pta, 25 )
	fmt.Fprintf( os.Stderr, "%s\n", ptc )

}

func TestBBox( t *testing.T ) {
	pts := make( []*RealPoint, 0 )
	pts = append( pts, NewRealPoint( 125, 50 ) )
	pts = append( pts, NewRealPoint( 100, 100 ) )
	pts = append( pts, NewRealPoint( 175, 125 ) )
	pts = append( pts, NewRealPoint( 100, 0 ) )
	pts = append( pts, NewRealPoint( 200, 75 ) )

	min, max := BoundingBox( pts )

	x, y := min.XY()
	if x != 100 && y != 0 {
		fmt.Fprintf( os.Stderr, "bad min value: %d,%d\n", x, y )
		t.Fail()
	}
	x, y = max.XY()
	if x != 200 && y != 125 {
		fmt.Fprintf( os.Stderr, "bad max value: %d,%d\n", x, y )
		t.Fail()
	}
}
