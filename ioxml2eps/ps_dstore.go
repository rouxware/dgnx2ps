/*
	Abstract:	Postscript to draw a "datastorage" thing.

	Date:		7 May 2022
	Author:		E. Scott Daniels
*/

package ioxml2eps

import (
	"io"
)

var psDstoreWritten bool = false
var psDstoreCode string = `
% special skewed arc for datastorage dthing
% MUST be called within a gsave/restore pair
% x y width height  start-ang end-ang
/dsArcPath {
		/_e exch def  /_s exch def  /_h exch def  /_w exch def 
		translate
		1 _h _w div scale
		0 0 _w 2 div _s _e   
		arc
} def

% rgb is the fill colour
% r g b x y w h dstore
/dstore {
	snagXYWH	% parameters
	snagRGB

	_x _y moveto _w 2 sub 0 rlineto
	0 _h rlineto
	_w 2 sub neg 0 rlineto
	closepath
	gsave
		_r _g _b setrgbcolor
		fill
	grestore

	newpath
	_x _y 1 sub moveto _w 0 rlineto stroke
	_x _y _h add 1 add moveto _w 0 rlineto stroke

	gsave
		_x _w add _y _h 2 div add
		25 _h 90 270 dsArcPath
		gsave
			1 1 1 setrgbcolor
			fill
		grestore
		stroke
	grestore
	newpath
	
	gsave
		_x _y _h 2 div add 
		25 _h 90 270 dsArcPath
		gsave
			_r _g _b setrgbcolor
			fill
		grestore
		stroke
	grestore
} def
`

func psDstoreWrite( out io.Writer ) {
	if ! psDstoreWritten {
		psUtilWrite( out )	// ensure we are set up
		out.Write( squishPS( psDstoreCode, true ) )
		psDstoreWritten = true
	}
}

