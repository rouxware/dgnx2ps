
package ioxml2eps

import (
	"fmt"
	"os"
	"testing"
)

func TestPSWriting( t *testing.T ) {
	psRptWrite( os.Stderr )
	psRoundBoxWrite( os.Stderr )
	psBoxWrite( os.Stderr )
	psTangleWrite( os.Stderr )
	psCircleWrite( os.Stderr )
	psEllipseWrite( os.Stderr )
	psDiamondWrite( os.Stderr )
	psArcWrite( os.Stderr )
	psDiskWrite( os.Stderr )
	psWriterWrite( os.Stderr )

	fmt.Fprintf( os.Stderr, "100 100 150 100 rpt\n" )
	fmt.Fprintf( os.Stderr, "180 0 0 100 250 150 100 fbox\n" )
	fmt.Fprintf( os.Stderr, "250 0 0 100 350 150 100 frbox\n" )
	fmt.Fprintf( os.Stderr, "1 1 1 200 100 150 100 disk\n" )

	fmt.Fprintf( os.Stderr, "%% ----------------- nothing below this line --------\n" )

	/*
		NONE of these should output anything
	*/
	psRptWrite( os.Stderr )
	psRoundBoxWrite( os.Stderr )
	psBoxWrite( os.Stderr )
	psTangleWrite( os.Stderr )
	psCircleWrite( os.Stderr )
	psEllipseWrite( os.Stderr )
}

