/*
	Abstract:	Tests for the sketch module.
	Date:		17 March 2022
	Author:		E. Scott Daniels
*/

package ioxml2eps

import (
	"fmt"
	"os"
	"testing"
)

func TestXML( t *testing.T ) {
	fname := "test_data/dt_test.xml"
	drawing, err := NewSketch( fname )				// laod the drawing from disk
	abortIfTrue( t, err != nil, fmt.Sprintf( "xml file could not be loaded: %s", err ) )

	fmt.Fprintf( os.Stderr, "%% <INFO> drawing loaded from %s\n", fname )
	fmt.Fprintf( os.Stderr, "%% <INFO> %d pages %d elements\n", drawing.Pages(), drawing.EleCount() )

	ele := drawing.Element( 12 )
	abortIfTrue( t, ele == nil, "couldn't find element" )

	x, y := ele.XY() 
	fmt.Fprintf( os.Stderr, "%% <INFO> element position (%d, %d)\n", x, y )

	//fmt.Fprintf( os.Stderr, "%% >>>> %v\n", ele )
}

