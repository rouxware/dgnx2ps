package ioxml2eps

import (
	"fmt"
	"os"
	"testing"
)

func TestParseStyle( t *testing.T ) {
	styleStr :=`aspect=fixed; labelPosition=left; verticalLabelPosition=middle;
			align=left; verticalAlign=middle; fontColor=#ffffff; fontSize=10;
			fillColor=#e51400; strokeColor=#B20000;`

	s := parseStyle( styleStr, true, "=" )
	if failIfTrue( t, s == nil, "didn't allocate style" ) {
		return
	}

	c := s.FontColour()
	fmt.Fprintf( os.Stderr, "%% <INFO> font colour = %q\n", c )
	failIfTrue( t, c != "1.00 1.00 1.00", "font colour wrong" )

	c = s.LineColour()
	fmt.Fprintf( os.Stderr, "%% <INFO> line colour = %q\n", c )
	failIfTrue( t, c != "0.70 0.00 0.00", "line colour wrong" )

	c = s.FillColour()
	fmt.Fprintf( os.Stderr, "%% <INFO> fill colour = %q\n", c )
	failIfTrue( t, c != "0.90 0.08 0.00", "fill colour wrong" )

	a := s.Align()
	fmt.Fprintf( os.Stderr, "%% <INFO> align= %d want %d\n", a, alignLeft )
	failIfTrue( t, a != alignLeft, "align wrong" )

	a = s.VAlign()
	fmt.Fprintf( os.Stderr, "%% <INFO> valign= %d want %d\n", a, alignMiddle )
	failIfTrue( t, a != alignMiddle, "vert align wrong" )

	p := s.LabelPos()
	fmt.Fprintf( os.Stderr, "%% <INFO> labelpos= %d want %d\n", p, posLeft )
	failIfTrue( t, p != posLeft, "label position align wrong" )

	sz := s.FontSz()
	failIfTrue( t, sz != 10, "font size align wrong" )
}			

/*
	Using a set of styles captured from raw XML, create styles and extract
	the perceived kind to ensure we get what we think we should.
*/
func TestDTKind( t *testing.T ) {
	styles := make( map[string]string )
	expect := make( map[string]string )	// what we expect if NOT the same string in styles

	styles["oval"] = "ellipse;whiteSpace=wrap;html=1;aspect=fixed;"
	styles["rect"] = "whiteSpace=wrap;html=1;aspect=fixed;"

	styles["diamond"] = "rhombus;whiteSpace=wrap;html=1;"	// internally diamonds are labeld as rhombus
	expect["diamond"] = "rhomb"

	styles["roundRect"] = "rounded=1;whiteSpace=wrap;html=1;"
	styles["parallelogram"] = "shape=parallelogram;perimeter=parallelogramPerimeter;whiteSpace=wrap;html=1;fixedSize=1;"
	styles["dashedLine"] = "endArrow=none;dashed=1;html=1;rounded=0;"
	styles["line"] = "endArrow=none;html=1;rounded=0;"

	styles["dashedLine"] = "endArrow=none;dashed=1;html=1;dashPattern=1 3;strokeWidth=2;rounded=0;"
	expect["dashedLine"] = "line"

	styles["startArrow"] = "endArrow=classic;startArrow=classic;html=1;rounded=0;"
	expect["startArrow"] = "line"

	styles["endArrow"] = "endArrow=classic;html=1;rounded=0;"
	expect["endArrow"] = "line"

	for k, str := range styles {
		s := parseStyle( str, true, "=" )
		kind := s.DtKind()

		eStr, ok := expect[k]	// if a different expected string, use it
		if ! ok {
			eStr = k			// else default to expecting the same as the key
		}
		fmt.Fprintf( os.Stderr, "%% <INFO> DthingKind: wanted %s got %s\n", eStr, kind )
		failIfTrue( t, eStr != string(kind), fmt.Sprintf( "for key %q the expected kind %q was not what we got: %q",
			k, eStr, string(kind) ) )

	}
}

