/*
	Abstract:	Postscript colour management functions

	Date:		7 May 2022
	Author:		E. Scott Daniels
*/

package ioxml2eps

import (
	"io"
)

var psColourWritten bool = false
var psColourCode string = `
% given r g b pct lighten the colour by pct leaving r g b on the stack
/lighter {
	/_p exch def
	dup _p mul add 3 -1 roll
	dup _p mul add 3 -1 roll
	dup _p mul add 3 -1 roll
} def

% given r g b pct darken the colour by pct leaving r g b on the stack
/darker {
	/_p exch def
	dup _p mul sub 3 -1 roll
	dup _p mul sub 3 -1 roll
	dup _p mul sub 3 -1 roll
} def
`
func psColourWrite( out io.Writer ) {
	if ! psColourWritten {
		out.Write( squishPS( psColourCode, true ) )
		psColourWritten = true
	}
}

