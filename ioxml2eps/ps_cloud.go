/*
	Abstract:	Postscript to support drawing a cloud.
				The cloud is designed round a bounding box of 200x150. It is
				scaled accordingly if the user supplies a different set of
				dimensions.  It is created by drawing a series of overlapping
				circles which are filled leaving only the outlined edges,
				where there is no overlap, visible.

				The rgb on the postscript command is the fill colour; the
				current rgb setting is assumed to be the outline colour, so
				we must set it.

	Date:		30 May 2022
	Author:		E. Scott Daniels
*/

package ioxml2eps

import (
	"io"
)

var psCloudWritten bool = false
var psCloudCode string = `
	% fill(rgb) x y width height
	/cloud {
		/_h exch def
		/_w exch def
		/_y exch def
		/_x exch def
		/_b exch def
		/_g exch def
		/_r exch def

		gsv
			_x _y translate
			_w 200 div _h 150 div scale		% cloud ellipses are based on a bb of 200x150; scale accordingly
			3 setlinewidth
			53 -40 80 80 ellipse 		% this sets up the outline in the current colour
			40 -60 80 80 ellipse 
			55 -95 80 80 ellipse 
			90 -110 80 80 ellipse 
			139 -96 80 80 ellipse 
			160 -72 80 80 ellipse 
			112 -46 80 80 ellipse 

			0 setlinewidth				% no outline
			_r _g _b setrgbcolor		% and ensure colour is fill just in case
			_r _g _b 53 -40 80 80 fellipse 		% this sets up the outline in the current colour
			_r _g _b 40 -60 80 80 fellipse 
			_r _g _b 55 -95 80 80 fellipse 
			_r _g _b 90 -110 80 80 fellipse 
			_r _g _b 139 -96 80 80 fellipse 
			_r _g _b 160 -72 80 80 fellipse 
			_r _g _b 112 -46 80 80 fellipse 
		grst
	} def
`

func psCloudWrite( out io.Writer ) {
	if ! psCloudWritten {
		psUtilWrite( out )	// ensure we are set up
		out.Write( squishPS( psCloudCode, true ) )
		psCloudWritten = true
	}
}
