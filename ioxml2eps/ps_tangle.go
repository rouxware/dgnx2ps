/*
	Abstract:	Postscript to support drawing triangles.

	Date:		25 April 2022
	Author:		E. Scott Daniels
*/

package ioxml2eps

import (
	"io"
)


var psTangleWritten bool = false
var psTangleCode string = `
	% triangle -- just set the path; caller strokes or fills
	% x y height base
	/tangle {
		4 2 roll moveto
		/_b exch def
		_b 2 div neg exch neg rlineto
		_b 0 rlineto
		closepath
	} def

	% rotated triangle
	% x y height base rotation
	/rtangle {
		gsave
			5 3 roll translate
			rotate
			0 0 4 2 roll
			tangle	
			stroke
		grestore
	} def

	% filled triangle
	% r g b x y height base
	/ftangle {
		gsave
			4 copy tangle
			7 4 roll
			setrgbcolor fill
		grestore
		tangle stroke
	} def

	% rotated filled triangle 
	% r g b x y height base rotation
	/rftangle {
		gsave
			5 3 roll translate
			rotate
			gsave 
				5 2 roll setrgbcolor
				2 copy
				0 0 4 2 roll
				tangle	
				fill
			grestore

			0 0 4 2 roll tangle stroke
		grestore
	} def

	% --- right triangle support ----------------------
	% triangle -- just set the path; caller strokes or fills
	%           |\
    %         h | \
    %           |  \
    %           ----
    %           base
 	%
	% x y height base
	% sets the path; caller must invoke fill or stroke
	/rtTangle {
		/_b exch def
		/_h exch def
		moveto
		0 _h neg rlineto
		_b 0 rlineto closepath
	} def

	% rotated right triangle; charpath only, caller must enclose
	% in gsave/restore
	%
	% x y height base rotation
	/rRtTangle {
		5 3 roll translate
		rotate
		0 0 4 2 roll
		rtTangle	
	} def

	% right triangle, filled and stroked, no rotaton
	% r g b x y height base
	/frtTangle {
		gsave
			0 rRtTangle
			gsave
				setrgbcolor
				fill
			grestore
			stroke
		grestore
	} def

	% right triangle, filled only, no rotation
	% r g b x y height base
	/foRtTangle {
		gsave
			0 rRtTangle
			setrgbcolor
			fill
		grestore
	} def

	% rotated right triangle; filled and stroked
	% r g b x y height base rotation
	/rfRtTangle {
		gsave
			gsave
				rRtTangle
				setrgbcolor
				fill
			grestore
			stroke
		grestore
	} def 

	% right triangle, filled only, with rotation
	% r g b x y height base rotation
	/rfoRtTangle {
		gsave
			rRtTangle
			setrgbcolor
			fill
		grestore
	} def
`

func psTangleWrite( out io.Writer ) {
	if ! psTangleWritten {
		psUtilWrite( out )	// ensure we are set up
		out.Write( squishPS( psTangleCode, true ) )
		psTangleWritten = true
	}
}
