/*
	Abstract:	Postscript to support drawing circles.

	Date:		25 April 2022
	Author:		E. Scott Daniels
*/

package ioxml2eps

import (
	"io"
)

var psCircleWritten bool = false
var psCircleCode string = `
	% circle
	% x y radius
	/circ {
		0 360 arc	
	} def

	% filled circle
	% r g b x y radius
	/fcirc {
		gsave
			6 3 roll setrgbcolor
			3 copy circ fill
		grestore
		circ stroke
	} def

	/focirc {
		gsave
			6 3 roll setrgbcolor
			circ fill
		grestore
	} def

	% shadow circ
	% r g b x y radius
	/scirc  {
		3 copy shadow 
		6 3 roll
		focirc
		gsave
			6 6 translate
			fcirc
		grestore
	} def
`

func psCircleWrite( out io.Writer ) {
	if ! psCircleWritten {
		psUtilWrite( out )	// ensure we are set up
		out.Write( squishPS( psCircleCode, true ) )
		psCircleWritten = true
	}
}


