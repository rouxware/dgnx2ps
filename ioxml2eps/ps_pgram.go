/*
	Abstract:	Postscript to draw a parallelogram thing.

	Date:		25 April 2022
	Author:		E. Scott Daniels
*/

package ioxml2eps

import (
	"io"
)

var psPgramWritten bool = false
var psPgramCode string = `
	% parallelogram path
	% x y width height skew
	% parallelogram
	% x y width height skew
	/pgram {
		/_pgskew ed
		newpath
		4 2 roll moveto
		exch dup 0 rlineto
		exch _pgskew exch rlineto
		neg 0 rlineto
		closepath
	} def

	% filled parallelogram
	% r g b x y width height skew
	/fpgram {
		5 copy pgram
		gsave
			8 5 roll
			setrgbcolor	
			fill
		grestore
		pgram
		stroke
	} def

	% fill only parallelogram
	% r g b x y width height skew
	/fopgram {
		pgram
		gsave
			setrgbcolor
			fill
		grestore
	} def
`

func psPgramWrite( out io.Writer ) {
	if ! psPgramWritten {
		psUtilWrite( out )	// ensure we are set up
		out.Write( squishPS( psPgramCode, true ) )
		psPgramWritten = true
	}
}
