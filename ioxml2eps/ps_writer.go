/*
	Abstract:	Postscript to support writing strings with alighment.

	Date:		26 April 2022
	Author:		E. Scott Daniels
*/

package ioxml2eps

import (
	"io"
)

var psWriterWritten bool = false
var psWriterCode  string = `
	% some short defs to reduce overall size
	/ssf { scalefont setfont } def

	% move to and save the new point for later use
	/mts {
		/_lasty exch def	% capture the absolute point first
		/_lastx exch def
		_lastx _lasty
		moveto
	} def

	% rmove to and save the point
	/rms {
		rmoveto				% move there first
		currentpoint		% then capture point
		/_lasty exch def
		/_lastx exch def
	} def

	% go to the last saved point
	/lmoveto {
		_lastx _lasty moveto
	} def

	% go to the last save point and add param to the y value
	/lmplus {
		lmoveto 0 exch rms
	} def

	% go to the last save point and add move relative based on
	% the parameters and save
	/lmplusXY {
		lmoveto rms
	} def

	% concat two strings on the top of the stack such that
	% <str1> <str2> sCat -> <str1str2>
	%
	/sCat {
		1 index length /s1l exch def	% string 1 length
		dup length		% s2 length
		s1l add			% total len of concat
		/tl exch def	% total len for both
		tl string		% create new string
		dup				% put interval needs a "target"
		0 5 -1 roll putinterval
		dup				% and create a final target
		s1l				% offset for the second string
		4 -1 roll		% string to top and put in
		putinterval	
	}def

	% these define the show string (ss) funciton and then
	% invoke the workhorse writeSSTxt to do the dirty work
	% calling the ss function to do the right thing.
	%
	% writeTxt <string> <width> <function-name>

	% centerd
	/writeCTxt {
		/ss {
			exch dup stringwidth pop
			3 -1 roll exch sub 2 div 0 rms show	
		} def
		writeSSTxt
	} def

	% right aligned
	/writeRTxt {
		/ss { 
			exch dup stringwidth pop
			3 -1 roll exch sub 0 rms show
		} def
		writeSSTxt
	} def

	% left aligned
	/writeTxt {
		/ss {
			pop show
		} def
		writeSSTxt
	} def

	% Split the string on blanks such that each substring fits
	% inside of the given width. Each subsring is written either
	% left, right aligned or centered depending on the current
	% definition of the ss function.
	% ss funciton that shows the substring either left, right
	% justified or centered.
	%
	% (string) <width> <fontsize>
	/writeSSTxt {
		32 dict begin
		/_fs exch -1 mul def 
		/_w exch def	% line width
		/_t 0 def		%total space used
		/_i 0 def		% line number for move to
		/_str () def	% substring that fits

		gsave
		{
			( ) search
			/_state exch def
			dup stringwidth pop
			/_ww exch def	% word width
			_w _ww _t add le {
				_str _w ss
				grestore
				gsave
				/_i _i 1 add def
				0 _i _fs mul rms
				/_t _ww def			% reset width is now just the last word
				/_str exch def
				_state false eq { 	% this was the last word, write it too
					_str _w ss
					exit 
				} if
				/_str _str ( ) sCat def
				pop					% clear the sep left on stack
			}
			% else 
			{
				_str exch sCat		% add to string
				/_str exch def
				_state {			% add trailing space if there
					_str exch sCat
					/_str exch def
				} if

				/_t _str stringwidth pop def
				_state false eq { 	% this was the last word, write it too
					_str _w ss
					exit 
				} if
			} ifelse
		} loop
		grestore
		end
	} def
`

func psWriterWrite( out io.Writer ) {
	if ! psWriterWritten {
		psUtilWrite( out )	// ensure we are set up
		out.Write( squishPS( psWriterCode, true ) )
		psWriterWritten = true
	}
}

