/*
	Abstract:	Postscript to draw a report thing.

	Date:		25 April 2022
	Author:		E. Scott Daniels
*/

package ioxml2eps

import (
	"io"
)

var psrptWritten bool = false
var psRptCode string = `
% lay out the path which then can be stroked or filled
% x y width height rpt (x,y is lower left)
/rptPath {
	/_h exch def  % capture parameters
	/_w exch def
	/_y exch def
	/_x exch def

	/_c1x _w .75 mul  _x add def  % set up curve control points
	/_c1y _y def
	/_c2x _w .25 mul  _x add def
	/_c2y _h .25 mul _y add def
	/_c3x _w _x add def
	/_c3y _h .25 mul _y add def

	_c3x _c3y moveto
	0 _h rlineto
	_w neg 0 rlineto
	0 _h _h .2 mul add neg rlineto
	_c1x _c1y _c2x _c2y _c3x _c3y curveto
} def

% draw the outline of the report thing
% x y width height rpt
/rpt {
	rptPath
	stroke
} def

% fill the report thing
% r g b x y width height frpt
/frpt {
	gsv
	rptPath
	setrgbcolor fill
	grst
	
} def
`
func psRptWrite( out io.Writer ) {
	if ! psrptWritten {
		psUtilWrite( out )	// ensure we are set up
		out.Write( squishPS( psRptCode, true ) )
		psrptWritten = true
	}
}
