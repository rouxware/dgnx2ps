/*
	Abstract:	Postscript to support drawing boxes of various kinds.

	Date:		25 April 2022
	Author:		E. Scott Daniels
*/

package ioxml2eps

import (
	"io"
)

var (
	psRoundBoxWritten bool = false
	psBoxWritten bool = false
) 

var psRoundBoxCode  string = `
	% define a rounded box path
	% llx lly width height
	/rndbox {
		dup
		4 div
		dup 18 ge {
			pop /_radius 18 def	
		}
		% else
		{
			dup 5 lt {
				pop /_radius 5 def
			}
			% else
			{
				/_radius exch def
			} ifelse
		} ifelse
		newpath
		/_h ed /_w ed /_y ed /_x ed
		_x _radius add _y _radius add _radius 180 270 arc
		_x _w add _radius sub _y _radius add _radius 270 360 arc
		_x _w add _radius sub _y _h add _radius sub _radius 0 90 arc
		_x _radius add _y _h add _radius sub _radius  90 180 arc
		closepath
	} def

	% filled rounded box
	% r g b x y width height
	/frbox  {
		gsave
			4 copy rndbox
			7 4 roll
			setrgbcolor
			fill
		grestore
		rndbox 
		stroke
	} def

	% fill only rounded box (no stroke)
	/forbox  {
		gsave
			rndbox
			setrgbcolor
			fill
		grestore
	} def

	% set the path to the box
	% llx lly width height box
	/box { newpath 
		4 2 roll 
		moveto 2 copy 
		exch 0 rlineto 
		0 exch rlineto 
		exch neg 0 rlineto pop closepath 
	} def
`

var psBoxCode string = `
	% filled box
	% r g b x y width height
	/fbox  {
		gsave
			4 copy box
			7 4 roll
			setrgbcolor
			fill
		grestore
		box 
		stroke
	} def

	% fill only box (no stroke)
	/fobox  {
		gsave
			box
			setrgbcolor
			fill
		grestore
	} def

	% filled box with a shadow
	/sbox {
		gsave
			4 copy 
			shadow
			7 3 roll
			fobox
		grestore
		gsave
			6 6 translate
			fbox
		grestore
	} def

`

func psRoundBoxWrite( out io.Writer ) {
	if ! psRoundBoxWritten {
		psUtilWrite( out )	// ensure we are set up
		out.Write( squishPS( psRoundBoxCode, true ) )
		psRoundBoxWritten = true
	}
}


func psBoxWrite( out io.Writer ) {
	if ! psBoxWritten {
		psUtilWrite( out )	// ensure we are set up
		out.Write( squishPS( psBoxCode, true ) )
		psBoxWritten = true
	}
}
