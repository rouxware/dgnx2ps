
/*
	Abstract:	Bloody draw.io "values" are HTML.  WTF?  As if XML wasn't bad enough?
				These support parsing the value into a valid HTML (unescaped) string and
				then tokenising it to get out what should have just been encoded as an
				XMK object.   Where did these guys learn to code?

				To make matters worse, the HTML isn't even fully formed at times with 
				some values being just text<br>text with no <div> or <style> pair
				of opening/closing tags.  Come on, if you're going to embed HTML
				encapsulate all of it so that the parser doen't have to have odd
				exceptions.

				Then.... text over the line is aligned "bottom" and text below the line
				is labeled top. Grrrr

				To make matters worse, had newlines added in the drawing change the
				"style" of crap laid down in the value. Sigh.

	Date:		31 March 2022
	Author:		E. Scott Daniels
*/

package ioxml2eps

import (
"fmt"
	"io"
"os"
	"golang.org/x/net/html"
	"strings"
)


/*
value="&lt;div&gt;&lt;b style=&quot;font-size: 16px&quot;&gt;Magic&lt;/b&gt;&lt;/div&gt;&lt;span style=&quot;font-size: 16px&quot;&gt;&lt;div&gt;&lt;b&gt;Circle&lt;/b&gt;&lt;/div&gt;&lt;/span&gt;"

value="This is a muti line&lt;br&gt;text bit added to&amp;nbsp;&lt;br&gt;the box with hard&lt;br&gt;line breaks where&lt;br&gt;we want them!"

*/

func unescapeHTML( goop string ) string {
	return html.UnescapeString( goop )
}

/*
	Manages nested tags tracking things like whether or not the style
	needs to be popped when the matching end is seen.
*/
type tagBlk struct {
	popAtEnd	bool		// if true, the style stack is poppsed with end tag
	name		string
	brk			bool		// if true next text is not appended
	strs		[]string	// the accumulated text strings to print on close
}

/*
	Given a style object, extract the things that we need to set up to print
	a bit of text.
*/
func style2Eps( ss *StyleStack ) string {
	if ss == nil {
		return ""
	}

	rs := ""
	if v, found := ss.Value( skFontColour ); found {
		c := NewColour( v )
		rs += fmt.Sprintf( "%s setrgbcolor ", c )
		//fmt.Fprintf( os.Stderr, "%% >>>>> setting font colour based on: %s (%s)\n", v, c  )
		//ss.Dump()
	} 
	if v, found := ss.Value( skFont ); found {
		switch v {		// true font family, not what iodraw puts in as font family which we catch early
			case "Sans", "sans", "sansSerif", "SansSerif":
				v = "Helvetica"

			case "Sans-Bold", "sans-bold", "sansSerif-bold", "SansSerif-Bold":
				v = "Helvetica-Bold"

			case "Serif", "serif":
				v = "Times-Roman"

			case "Serif-Bold", "serif-bold":
				v = "Times-Roman-Bold"

			default:  // nothing, let v stand as set during parse
		}

		rs += fmt.Sprintf( "(%s) findfont ", v )
	} else {
		rs += fmt.Sprintf( "(Helvetica) findfont " )
	}

	if v, found := ss.Value( skFontSize ); found {
		fs := atoi( v ) 	// we assume pts; force any p or px suffix to go away
		if fs <= 0 {
			fs = 10
		}
		rs += fmt.Sprintf( "%d scalefont setfont", fs )
	} else {
		rs += fmt.Sprintf( "10 scalefont setfont " )
	}

	return rs
}

/*
	Write the strings out.  When called, we expect that a saving moveto has been done 
	to the x,y of the left margin/center of the thing. 

	Bloody hell.... why cant label alignment be top topLeft topRight... instead of having
	to have two parameters?   Half of this code wouldn't be needed or as confusing
	if it were.  Sigh.
		labelPosition=right;
		verticalLabelPosition=top;
		align=left;
		verticalAlign=bottom;
*/
func writeStrings( out io.Writer, curTag *tagBlk, width int, height int, ss *StyleStack ) {
	if len( curTag.strs ) > 0 {
		setup := style2Eps( ss )
		psFprintf( out, "%s\n", setup )
	}

	fontSize := 10	// post script function needs this
	if v, found := ss.Value( skFontSize ); found {
		fontSize = atoi( v )
	}

	//if height <= 0 {
		////height = fontSize
		//height = -5
	//}

	align := "center"       // seems default for align should be centered text
	if v, found := ss.Value( skAlign ); found {
		align = v
	}

	vAlign := "center"	// same for vert; should be centered, but alas...
	if v, found := ss.Value( skVertAlign ); found {
		vAlign = v
	}
	vlPos := "undefined"
	if v, found := ss.Value( skVertLabPos ); found {
		vlPos = v
	}
	lPos := "undefined"
	if v, found := ss.Value( skLabPos ); found {
		lPos = v
	}

	epsWrite := "writeTxt"	// default to writing left aligned text
	switch align {
		case "right", "Right":	// override with right aligned
			epsWrite = "writeRTxt"

		case "center", "Center":	// override with centered text
			epsWrite = "writeCTxt"
	}

	epsWrite = "writeTxt"	// default to writing left aligned text
	switch align {
		case "right", "Right":	// override with right aligned
			epsWrite = "writeRTxt"

		case "center", "Center":	// override with centered text
			epsWrite = "writeCTxt"
	}

	nStrs := len( curTag.strs )
	adjX := 0	// current x,y is center/middle adjust only if one of the alignments changes
	adjY := 0

	if vlPos == "undefined" {		// text goes inside of the thing
		switch vAlign { 				//position inside the thing
			case "middle", "Middle", "center", "Center":	// compute position of top row based on strings/2
				if nStrs > 1 {
					adjY = ((nStrs/2)-1) * fontSize
				} else {
					adjY = -((fontSize/2) - 2)
				}
	
			case "bottom", "bot", "Bottom", "Bot":
				if height > fontSize {
					adjY = -((height/2) - (nStrs * fontSize))
				} else {
					adjY = fontSize/2
				}
	
			//case "top", "Top":			// just position at the top of the thing
			default:
				//adjY = (height/2) - (fontSize + (fontSize/2))
				if height > fontSize {
					adjY = (height/2) - ((fontSize + fontSize/2))
				} else {
					adjY = -((fontSize/2)+2)
				}
		}
	} else {		// text goes under/over/beside the thing; x,y is already centered
		switch vlPos {
			case "bottom", "bot", "Bottom", "Bot":
				if height > fontSize {
					adjY = (height/2) -  fontSize
				} else {
					adjY = -(fontSize/2)
				}

			//case "top", "Top":
			default:
				adjY = (height/2) + ((fontSize/2) * ((nStrs+1)/2))
		}

		switch lPos {
			case "Left", "left":
				adjX =  -width

			case "Right", "right":
				adjX =  width
		}
	}

	if adjX + adjY != 0 {
		psFprintf( out, "%d %d lmplusXY\n", adjX, adjY )
	}

	for i, s := range curTag.strs {
		if i > 0 {
			psFprintf( out, "-%d lmplus ", fontSize ) 	// move to last plus font size
		}
		//s = strings.ReplaceAll( s, "&nbsp;", " " )
		s = unescapeHTML( s )
		psFprintf( out, "(%s) %d %d %s\n", psEscape(s), width, fontSize, epsWrite )
	}
}

/*
	Accept a value goop and write the EPS commands to print it. The assumption
	that an initial moveto has been written.

	Jump the hoops and dodge the hammer while  rumaging through the bloody HTML
	goop trying to dig out text and style. (Who would have thought that we'd need
	to write a bloody HTML parser to convert what should be a simple XML
	drawing to eps???)

	The base style is likely the stayle information that is taken from the
	drawing thing and is used as the default to build any embedded HTML
	style.

	When called, we expect that a saving moveto has been done to the x,y of the
	left margin/center of the thing. 
*/
func value2Eps( out io.Writer, goop string, baseStyle *Style, width int, height int ) {
	sr := strings.NewReader( goop )
	hr := html.NewTokenizer( sr )

	tStack := make( []*tagBlk, 1024 )	// simple stack for tag blocks
	tagIdx := 0
	curTag := &tagBlk{}

	ss := NewStyleStack( baseStyle )

	//fmt.Fprintf( out, "%% >>>>>>> printing value: %s\n", goop )
	//fmt.Fprintf( os.Stderr, "%% >>>>>>> printing value: %s\n", goop )
	done := false
	for ; ! done; {
		tokType := hr.Next()
		//fmt.Fprintf( os.Stderr, "   %% >>> token: %v\n", tokType )
		switch tokType {
			case html.ErrorToken:
				//fmt.Fprintf( os.Stderr, "%% token: error token\n" )
				done = true

			case html.TextToken:
				r := hr.Raw()
				 //fmt.Fprintf( os.Stderr, "    %% >>> token: text %s\n", r)
				if curTag.strs == nil {
					curTag.strs = make( []string, 0 )
				}

				lsl := len(curTag.strs)-1
				if curTag.brk || lsl <= 0 {
					curTag.strs = append( curTag.strs, string(r) )
					curTag.brk = false
				} else {
					s := curTag.strs[lsl] + " " + string(r)
					curTag.strs[lsl] = s
				}

			case html.SelfClosingTagToken:
				name, _ := hr.TagName()
				switch string( name ) {
					case "br": curTag.brk = true
				}

			case html.EndTagToken:
				r := hr.Raw()
				name, _ := hr.TagName()
				//fmt.Fprintf( os.Stderr, "%% end token: %v  %s  name=%q curTag=%+v\n", tokType, string(r), string(name), curTag.strs )
				if string( name ) == "div" {		// we're ignoring divs here, so skip
					continue
				}
				if string(name) != curTag.name {
					fmt.Fprintf( os.Stderr, "%% ###ERR###: name mismatch on end tag: end=%s cur=%s\n", r, curTag.name)
				}
				writeStrings( out, curTag, width, height, ss )

				if curTag.popAtEnd {
					//fmt.Fprintf( os.Stderr, "%% pop style\n" )
					ss.Pop()
				}
				if tagIdx > 0 {
					tagIdx--
					curTag = tStack[tagIdx]
				} else {
					fmt.Fprintf( os.Stderr, "\n### ABEND ### tag stack underflow in html parser\n\n" )
				}

			case html.StartTagToken:
				name, hasAttr := hr.TagName()
				 //fmt.Fprintf( os.Stderr, "   %% >>>> token: %v name=%q hasAttr=%v\n", tokType, string(name), hasAttr )

				switch string(name) {				// some tags like <br> don't have a close (and should really be <br />!)
					case "div", "br", "p":			// treat <p> the same way, and bloody dio uses <div>string</div> instead of <br>; wtf???
						curTag.brk = true
						continue
				}

				if tagIdx >= len(tStack) {
					fmt.Fprintf( os.Stderr, "\n### ABEND ### tag stack overflow in html parser\n\n" )
					os.Exit( 1 )
				}

				tStack[tagIdx] = curTag
				tagIdx++
				curTag = &tagBlk {
					name: 	string(name),
					strs:	make( []string, 0),
				}

				if hasAttr {
					for {
						key, val, more := hr.TagAttr()
						if string(key) == "style" {
							curTag.popAtEnd = true
							style := parseStyle( string(val), false, ":" )	// parse with HTML separator not key=val
							ss.Push( style )
						}
						if ! more {
							break
						}
					}
				}

			default:
				fmt.Fprintf( os.Stderr, "%% ###ERR###  unknown token type: %v\n", tokType )
		}
	}

	writeStrings( out, curTag, width, height, ss )		// some HTML doesn't have an end tag to force the last write :(
}
