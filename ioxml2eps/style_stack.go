/*
	Abstract:	Manages a stack of style objects for easily dealing with
				nested styles in the bloody HTML that io.draw puts into the
				"value" field of an object.

	Date:		1 April 2022 (I aint nobody's fool!)
	Author:		E. Scott Daniels
*/

package ioxml2eps
import (
	"fmt"
	"os"
)


/*
	The stack is a first on first off stack.
*/
type StyleStack struct {
	stack	[]*Style
	curIdx	int
}

func NewStyleStack( defStyle *Style ) *StyleStack {
	ss := &StyleStack {
		stack: make( []*Style, 64 ),		// we can grow it, but this should be more than enough
	}

	ss.stack[0] = defStyle		// the base (not popable) is the "default"

	return ss
}

/*
	Clear the whole stack keeping the base (not removable)
*/
func (ss *StyleStack) Clear() {
	if ss == nil {
		return
	}

	newStack := make( []*Style, 64 )		// we can grow it, but this should be more than enough
	newStack[0] = ss.stack[0]
	ss.stack = newStack
	ss.curIdx = 0
}

/*
	Returns the number of things on the stack.
	Mostly for testing.
*/
func (ss *StyleStack) Len() int {
	if ss == nil {
		return 0
	}
	return ss.curIdx + 1
}

/*
	Pop the most recently added style from the stack. The base style cannot
	be popped.
*/
func (ss *StyleStack) Pop() {
	if ss == nil {
		return
	}

	if ss.curIdx > 0 {
		ss.stack[ss.curIdx] = nil
		ss.curIdx--
	}
}

/*
	Push the style onto the top of the stack. If the stack size needs to be increased,
	it will be.
*/
func (ss *StyleStack) Push( new *Style ) {
	if ss == nil {
		return
	}

	ss.curIdx++
	if ss.curIdx == cap( ss.stack ) {
		newStack := make( []*Style, cap( ss.stack ) + 64 )
		for i := 0; i < ss.curIdx; i++ {
			newStack[i] = ss.stack[i]
		}
		ss.stack = newStack
	}

	ss.stack[ss.curIdx] = new
}

/*
	Return the value for the given key. Styles stack, and we return the
	most recently defined style on the stack staring with the top style.
	If the style isn't defined on the stack, then the booliean returned
	is set to false.
*/
func (ss *StyleStack) Value( sKey StyleKey ) (string, bool) {
	isThere := false
	val := ""
	for i := ss.curIdx; i >= 0 && !isThere; i-- {
		val, isThere = ss.stack[i].Value( sKey )
	}

	return val, isThere
}

func (ss *StyleStack) Dump() {
	if ss == nil {
		return
	}

	for i := ss.curIdx; i >= 0; i-- {
		st := ss.stack[i]
		if st != nil && st.settings != nil {
			for k, v := range st.settings {
				fmt.Fprintf( os.Stderr, "%% style [%d]: %s = %q\n", i, k, v )
			}
		}
	}
}
